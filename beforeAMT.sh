#!/bin/bash

currpath=$1/r$2 #first input is the topic name, second input is the round number
toppath=$1

touch $toppath/whitelist.csv #make sure these exist 
touch $toppath/irrelevant.csv

python numberbatch.py $currpath/seeds.csv > $currpath/numberbatch_results.csv

python removeDuplicates.py $toppath/whitelist.csv $toppath/irrelevant.csv $currpath/numberbatch_results.csv $currpath/newterms.csv

python prepareForMT.py $currpath/newterms.csv $currpath/forAMT_.csv #scp for_AMT.csv to local and upload to AMT

cat $currpath/forAMT_.csv | sed  "s/\/neg//g" > $currpath/forAMT.csv #remove the annoying /negs

rm $currpath/forAMT_.csv


