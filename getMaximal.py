# USAGE:
# python getMaximal.py [infile]
# Input: Given a file of words,relevances
# Output: Prints the ones of highest relevance (for sampling later)

import sys

with open(sys.argv[1]) as f:
    for line in f:
        line = line.split(',')
        word = line[0]
        relevance = float(line[1])
        slant = line[2].strip('\n')
        if relevance >= 2.9:
            print(word + ',' + str(relevance) + ',' + slant)
