# USAGE:
# python sampleRandom.py [infile] [outfile]
# Input: infile, a file with all on-topic words
# Output: outfile, a file of a random sample of up to 5 terms from each category

import sys, random

pos = []
neg = []
neu = []
with open(sys.argv[1]) as f:
    for line in f:
        line = line.split(',')
        word = line[0]
        rel = float(line[1])
        slant = line[2].strip('\n')

        if (rel > 2.5):
            if (slant == "pos"):
                pos.append(word)
            elif (slant == "neg"):
                neg.append(word)
            else:
                neu.append(word)

with open(sys.argv[2], 'w') as f:
    for lst in [pos, neg, neu]:
        if len(lst) >= 5:
            #sample = [ lst[i] for i in sorted(random.sample(xrange(len(lst)), 5)) ] #randomly sample 5 terms
            random.shuffle(lst)
            sample = lst[:5]
            for item in sample:
                f.write(item + '\n' )
        else:
            for item in lst:
                f.write(item + '\n')
