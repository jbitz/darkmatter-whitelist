# USAGE:
# python removeDuplicates.py [existingWhitelist] [irrelevants] [fileToDedup] [outfile] 
# Input: the existing whitelist and a csv of new whitelist candidate terms
# Output: in the same numberbatch format, only those words we haven't seen before

import sys


# load in the whitelist
whitelist = []
with open(sys.argv[1]) as f:
#    print "WHITELIST***************************"
    for line in f:
        word = line.split(',')[0]
        word = word.strip('\n') #chop off \n if there
#        print word
        whitelist.append(word)
            
irrelevant = []
with open(sys.argv[2]) as f:
#    print "IRRELEVANT***************************"
    for line in f:
        word = line.strip('\n') #chop off \n
        irrelevant.append(word)

# read through the new stuff
newwords = []
with open(sys.argv[3]) as f:
#    print "NEW***************************"
    for line in f:
        word = line[6:line.find(',')]#chop of the c/en/ at the start and the parent term at the end
        if (word not in whitelist) and (word not in irrelevant) and (word not in newwords):
#            print word
            newwords.append(line[:-1])

with open(sys.argv[4], 'w') as f:
    for word in newwords:
        f.write(word + '\n')
