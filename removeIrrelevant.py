# USAGE:
# python removeIrrelevant.py [infile] [outfile_relevant] [outfile_irrelevant]
# Input: file from AMTurk with two ratings for each word on relevance and slant
#        + the iteration step (for labeling the output files)
# Output: a file with words and relevances, another file with the irrelevant words

import sys

positive = []
negative = []
neutral = []
irrelevant = []

def processRelevanceString(s):
    s = s.strip().replace('"', '')
    if s == '': return 0
    else: return int(s)


with open(sys.argv[1]) as f:
    header = (next(f)).split(',')#get header and skip that line in subsequent loop
    relIndex = [index for index, value in enumerate(header) if value == '"Answer.Relevance"'][0]
    slantIndex = [index for index, value in enumerate(header) if value == '"Answer.Attitude"'][0]
    wordIndex = [index for index, value in enumerate(header) if value == '"Input.word"'][0]

    for line in f:
        line = line.split('",') #needs the quote char because there might be commas in a string somwhere esle                                            
        word = line[wordIndex].replace('"', '')
        rel = processRelevanceString(line[relIndex])
        slant = line[slantIndex].replace('"', '')

        if (rel == 1):
            slant='Irrelevant'

        if (slant == 'Positive'):
            positive.append((word,rel))
        if (slant == 'Negative'):
            negative.append((word, rel))
        if (slant == 'Neutral'):
            neutral.append((word, rel))
        if (slant == 'Irrelevant'):
            irrelevant.append(word)

with open(sys.argv[2], 'w') as f:

    for (word,rel) in positive:
        f.write(word + ',' + str(rel) + ',pos' + '\n')

    for (word,rel) in negative:
        f.write(word + ',' + str(rel) + ',neg' + '\n')

    for (word,rel) in neutral:
        f.write(word + ',' + str(rel) + ',neu' + '\n')

with open(sys.argv[3], 'w') as f:
    for word in irrelevant:
        f.write(word + '\n')
