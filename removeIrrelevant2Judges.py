# USAGE:
# python removeIrrelevant.py [infile] [outfile_relevant] [outfile_irrelevant]
# Input: file from AMTurk with two ratings for each word on relevance and slant
#        + the iteration step (for labeling the output files)
# Output: a file with words and relevances, another file with the irrelevant words

import sys

positive = []
negative = []
neutral = []
irrelevant = []

def processRelevanceString(s):
    s = s.strip().replace('"', '')
    if s == '': return 0
    else: return int(s)


with open(sys.argv[1]) as f:
    header = (next(f)).split(',')#get header and skip that line in subsequent loop
    relIndex = [index for index, value in enumerate(header) if value == '"Answer.Relevance"'][0]
    slantIndex = [index for index, value in enumerate(header) if value == '"Answer.Attitude"'][0]
    wordIndex = [index for index, value in enumerate(header) if value == '"Input.word"'][0]

    for line in f:

        firstline = line.split('",') #needs the quote char because there might be commas in a string somwhere esle
        word = firstline[wordIndex].replace('"', '')
        secondline = next(f).split('",')
        word2 = secondline[wordIndex].replace('"', '')

        assert (word == word2)

        firstRel = processRelevanceString(firstline[relIndex])
        secondRel = processRelevanceString(secondline[relIndex])
        firstSlant = firstline[slantIndex].replace('"', '')
        secondSlant = secondline[slantIndex].replace('"', '')
        if(firstRel == 0): firstRel = secondRel
        if(secondRel == 0): secondRel == firstRel

        if (firstRel == 1 and secondRel == 1): #skip when both turkers say irrelevant
            finalSlant='Irrelevant'

        elif (firstRel == 1 or secondRel == 1): #if only one thinks irrelevant, trust the other turker
                finalRel = max(firstRel, secondRel)
                if (firstRel > secondRel):
                    finalSlant = firstSlant
                else:
                    finalSlant = secondSlant
        else: #tie-breaking on slant...
            finalRel = (firstRel + secondRel)/2.0
            if (firstSlant == secondSlant):
                finalSlant = firstSlant
            else:
                if (firstSlant == 'Neutral'): #side with non-neutral
                    finalSlant = secondSlant
                elif (secondSlant == 'Neutral'):
                    finalSlant = firstSlant
                else: #if one says PRO and the other ANTI, file it as both
                    finalSlant = 'Both'
        if (finalSlant == 'Positive'):
            positive.append((word,finalRel))
        if (finalSlant == 'Negative'):
            negative.append((word, finalRel))
        if (finalSlant == 'Neutral'):
            neutral.append((word, finalRel))
        if (finalSlant == 'Both'):
            positive.append((word,finalRel))
            negative.append((word,finalRel))
        if (finalSlant == 'Irrelevant'):
            irrelevant.append(word)

with open(sys.argv[2], 'w') as f:

    for (word,rel) in positive:
        f.write(word + ',' + str(rel) + ',pos' + '\n')

    for (word,rel) in negative:
        f.write(word + ',' + str(rel) + ',neg' + '\n')

    for (word,rel) in neutral:
        f.write(word + ',' + str(rel) + ',neu' + '\n')

with open(sys.argv[3], 'w') as f:
    for word in irrelevant:
        f.write(word + '\n')
